package lab.studies.client;

import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

import java.util.concurrent.Executors;

public class StudiesClient {

    public static void main(String[] args) {

        String url = "http://localhost:8080/studies/reactive";

        Flux<StudentDto> flux = WebClient.create(url)
                .get()
                .uri("/students")
                .headers(h->{
                    h.add("Accept", "text/event-stream");
                })
                .retrieve()
                .bodyToFlux(StudentDto.class);

        flux
                .doOnNext(s-> System.out.println("next:" + s))
                .doOnComplete(()-> System.out.println("flux complete"))
                .subscribe();
          //      .subscribe(System.out::println);


        Executors.newFixedThreadPool(1).execute(()-> {
            try {
                Thread.sleep(15*1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });
    }

    static class StudentDto{
        private int id;
        private String firstName;
        private String lastName;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        @Override
        public String toString() {
            return "StudentDto{" +
                    "id=" + id +
                    ", firstName='" + firstName + '\'' +
                    ", lastName='" + lastName + '\'' +
                    '}';
        }
    }
}
