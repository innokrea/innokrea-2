package lab.studies.service.reactive;

import lab.studies.reactive.dao.StudentReactiveDao;
import lab.studies.model.Student;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import java.time.Duration;

@Service
public class StudiesReactiveService {

    private final StudentReactiveDao studentReactiveDao;

    public StudiesReactiveService(StudentReactiveDao studentReactiveDao) {
        this.studentReactiveDao = studentReactiveDao;
    }

    public Flux<Student> getStudents(){
        return studentReactiveDao.findAll()
                .delayElements(Duration.ofSeconds(3));
    }
}
