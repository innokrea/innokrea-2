package lab.studies.reflection;

import lab.studies.model.Student;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

public class ReflectionMain {

    public static void main(String[] args) throws ClassNotFoundException {

        System.out.println("Let's reflect!");

        Object o = getObject();

        hijackObject(o, "id", 102);

        validate(o);

        Class<?> clazz = o.getClass();
                //Student.class;
                //Class.forName("lab.studies.model.Student");

        do {
            ClassViewer viewer = new ClassViewer(clazz);
            viewer.printClassInfo();
            viewer.printFieldsInfo();
            viewer.printConstructors();
            viewer.printMethodInfo();
            clazz = clazz.getSuperclass();
        }while (clazz!=Object.class);

    }

    static void validate(Object o){
        Class clazz = o.getClass();
        do {
            Arrays.stream(clazz.getDeclaredFields()).forEach(f -> {
                if (f.isAnnotationPresent(Positive.class)) {
                    f.setAccessible(true);
                    try {
                        Object value = f.get(o);
                        if (Number.class.isAssignableFrom(value.getClass())) {
                            long longValue = ((Number) value).longValue();
                            Positive positive = f.getAnnotation(Positive.class);
                            if (longValue < 0 || longValue > positive.max()) {
                                throw new IllegalArgumentException(
                                        "field " + f.getName() + " should be positive, actual value " + longValue);
                            }
                        }
                    } catch (IllegalAccessException e) {
                        throw new RuntimeException(e);
                    }
                }
            });
            clazz = clazz.getSuperclass();
        }while (clazz!=Object.class);
    }


    static Object getObject(){

        String className = "lab.studies.model.Student";
        int id = 1;
        String firstName = "Jan";
        String lastName = "Kowalski";

        try {
            Class clazz = Class.forName(className);
            Constructor constructor = clazz.getConstructor(int.class, String.class, String.class);
            Object o = constructor.newInstance(id, firstName, lastName);
            return o;

        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }

        //return new Student(1, "Jan", "Kowalski");
    }

    static void hijackObject(Object o, String fieldName, Object value){
        Class clazz = o.getClass();
        Field field = null;
        do {
            try {
                field = clazz.getDeclaredField(fieldName);
                break;
            } catch (NoSuchFieldException e) {
                clazz = clazz.getSuperclass();
            }
        }while (clazz!=Object.class);
        try {
            field.setAccessible(true);
            Object currentValue = field.get(o);
            System.out.println("currentValue = " + currentValue);
            field.set(o, value);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }

    }

}
