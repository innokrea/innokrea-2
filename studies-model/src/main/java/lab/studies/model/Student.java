package lab.studies.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "student")
public class Student extends Person {


/*
    @ManyToMany(mappedBy = "students", fetch = FetchType.EAGER)
    @JsonIgnore
    private List<Subject> subjects = new ArrayList<>();
*/

    public Student(int id, String firstName, String lastName) {
        super(id, firstName, lastName);
    }

    public Student() {
    }

   /* public List<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
    }
*/
    public void addSubject(Subject subject) {
        //this.subjects.add(subject);
        subject.addStudent(this);
    }

    @Override
    public String toString() {
        return "Student{" +
                " person=" + super.toString() +
                '}';
    }
}
