package lab.studies.reactive.dao;

import lab.studies.model.Student;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

public interface StudentReactiveDao extends ReactiveCrudRepository<Student, Integer> {

    Flux<Student> findAll();
}
