package lab.studies.dao;

import lab.studies.model.Teacher;

import java.util.List;
import java.util.Optional;

public interface TeacherDao {

    Teacher save(Teacher teacher);

    List<Teacher> findAll();

    Optional<Teacher> findById(int id);

}
