package lab.studies.dao;

import lab.studies.model.Subject;

import java.util.List;
import java.util.Optional;

public interface SubjectDao {

    Subject save(Subject subject);

    List<Subject> findAll();

    Optional<Subject> findById(int id);

    Optional<Subject> findByName(String name);

}
