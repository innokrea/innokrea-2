package lab.studies.dao.r2dbc;

import lab.studies.reactive.dao.StudentReactiveDao;
import lab.studies.model.Student;
import org.springframework.data.r2dbc.core.DatabaseClient;
import reactor.core.publisher.Flux;

import java.util.Map;
import java.util.function.Function;

//@Repository
public abstract class R2DBCStudentReactiveDao implements StudentReactiveDao {

    public static final Function<Map<String, Object>, Student> MAPPING_FUNCTION = row -> new Student(
            Integer.parseInt(row.get("id").toString()),
            row.get("first_name").toString(),
            row.get("first_name").toString());



    private final DatabaseClient databaseClient;

    public R2DBCStudentReactiveDao(DatabaseClient databaseClient) {
        this.databaseClient = databaseClient;
    }

    @Override
    public Flux<Student> findAll() {
        return databaseClient
                .execute(()->"SELECT * FROM student")
                .fetch()
                .all()
                .map(MAPPING_FUNCTION);

    }
}