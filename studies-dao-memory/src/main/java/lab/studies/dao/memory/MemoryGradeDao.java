package lab.studies.dao.memory;

import lab.studies.dao.GradeDao;
import lab.studies.model.Grade;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class MemoryGradeDao implements GradeDao {

    @Override
    public Grade save(Grade grade) {
        int maxId = MemoryData.grades.stream().mapToInt(t->t.getId()).max().orElse(0);
        grade.setId(maxId);
        MemoryData.grades.add(grade);
        return grade;
    }

    @Override
    public List<Grade> findAll() {
        return MemoryData.grades;
    }

    @Override
    public Optional<Grade> findById(int id) {
        return MemoryData.grades.stream().filter(s->s.getId()==id).findFirst();
    }
}
