package lab.studies.web.reactive;

import lab.studies.model.Student;
import lab.studies.service.reactive.StudiesReactiveService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import java.util.logging.Logger;

@RestController
@RequestMapping("/reactive")
public class StudiesReactiveRest {

    private static final Logger log = Logger.getLogger(StudiesReactiveRest.class.getName());

    private final StudiesReactiveService service;

    public StudiesReactiveRest(StudiesReactiveService service) {
        this.service = service;
    }

    @GetMapping(value = "/students")
    Flux<StudentDto> getStudents(){
        log.info("retrieving students stream");
        return service.getStudents()
                .map(this::toDto);
    }

    private StudentDto toDto(Student s) {
        StudentDto dto = new StudentDto();
        dto.setId(s.getId());
        dto.setFirstName(s.getFirstName());
        dto.setLastName(s.getLastName());
        return dto;
    }

    static class StudentDto{
        private int id;
        private String firstName;
        private String lastName;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }
    }
}
